import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EimageuploadComponent } from './eimageupload.component';

describe('EimageuploadComponent', () => {
  let component: EimageuploadComponent;
  let fixture: ComponentFixture<EimageuploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EimageuploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EimageuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
