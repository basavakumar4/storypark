import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'datepicker-popup',
  templateUrl: './editstory.component.html',
  styleUrls: ['./editstory.component.scss']
})
export class EditstoryComponent implements OnInit {
  event: Date = new Date(2018, 0, 30);

  datepickerConfig: Partial< BsDatepickerConfig >;
  
  constructor() { 
    this.datepickerConfig = Object.assign({},
       {
         containerClass: 'theme-dark-blue', 
         showWeekNumbers:false,
         minDate: new Date(2018, 0, 1),
         maxDate: new Date(2018, 11, 31),
         dateInputFormat: 'DD/MM/YYYY'
      });
  }
  Clickpopup(){
    document.getElementById("overlay").style.display = "block";
  }
  Clickclose(){
    document.getElementById("overlay").style.display = "none";
  }   
 
  ngOnInit() {
  }
 
onBodyTextEditorkeyUp(event){
  console.log("text is change:",event);
}
}
