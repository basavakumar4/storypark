import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtexteditorComponent } from './etexteditor.component';

describe('EtexteditorComponent', () => {
  let component: EtexteditorComponent;
  let fixture: ComponentFixture<EtexteditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtexteditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtexteditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
