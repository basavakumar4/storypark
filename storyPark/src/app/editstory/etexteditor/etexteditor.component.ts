import { Component,OnDestroy,AfterViewInit,EventEmitter,Input,Output, OnInit } from '@angular/core';

@Component({
  selector: 'text-editor',
  templateUrl: './etexteditor.component.html',
  styleUrls: ['./etexteditor.component.scss']
})
export class EtexteditorComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() elementId: String;
  @Output() onEditorKeyup = new EventEmitter<any>();

  constructor() { }
  ngOnInit() {
  }
  
editor;
  tinymce: any;

  ngAfterViewInit() {
      tinymce.init({
        menubar:false,
        statusbar: false,
        
        //theme : "advanced",  
          selector: '#' + this.elementId,
          plugins: ['colorpicker textcolor'],
          skin_url: 'assets/skins/lightgray',
          toolbar: "fontselect |  fontsizeselect | bold italic | alignleft aligncenter alignright | forecolor | bullist numlist",
          font_formats : 	"Arial=arial,helvetica,sans-serif;"+"Book Antiqua=book antiqua,palatino;"+ "Courier New=courier new,courier;"+"Georgia=georgia,palatino;"+"Helvetica=helvetica;",
          setup: editor => {
              this.editor = editor;
              editor.on('keyup', () => {
                  const content = editor.getContent();
                  this.onEditorKeyup.emit(content);
              });
          },
      });
  }

  ngOnDestroy() {
      this.tinymce.remove(this.editor);
  }
}