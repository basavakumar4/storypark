import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { PagesModule } from './pages/pages.module';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ActivityComponent } from './activity/activity.component';
import { AheaderComponent } from './activity/aheader/aheader.component';
import { AconversationComponent } from './activity/aconversation/aconversation.component';
import { AcommunityComponent } from './activity/acommunity/acommunity.component';
import { AstoryComponent } from './activity/astory/astory.component';
import { ApreviewComponent } from './activity/apreview/apreview.component';
import { EditstoryComponent } from './editstory/editstory.component';
import { EheaderComponent } from './editstory/eheader/eheader.component';
import { EtexteditorComponent } from './editstory/etexteditor/etexteditor.component';
import { EimageuploadComponent } from './editstory/eimageupload/eimageupload.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PagesModule,
    routing,
    HttpClientModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    AppComponent,
    ActivityComponent,
    AheaderComponent,
    AconversationComponent,
    AcommunityComponent,
    AstoryComponent,
    ApreviewComponent,
    EditstoryComponent,
    EheaderComponent,
    EtexteditorComponent,
    EimageuploadComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
