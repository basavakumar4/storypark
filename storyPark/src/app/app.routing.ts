import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { ActivityComponent } from './activity/activity.component';
import { EditstoryComponent } from './editstory/editstory.component';

// const appRoutes: Routes = [
//   {
//     path: 'login',
//     redirectTo: 'login',
//     pathMatch: 'full'
//   },
//   {
//     path: '**',
//     redirectTo: 'login'
//   }
// ];
const appRoutes: Routes  = [
  { path: '', redirectTo: '/activity', pathMatch: 'full' },
  { path: 'activity', component: ActivityComponent },
  { path: 'editstory', component: EditstoryComponent },
]

export const routing = RouterModule.forRoot(appRoutes);
