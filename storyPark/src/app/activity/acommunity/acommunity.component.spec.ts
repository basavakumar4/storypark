import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcommunityComponent } from './acommunity.component';

describe('AcommunityComponent', () => {
  let component: AcommunityComponent;
  let fixture: ComponentFixture<AcommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
