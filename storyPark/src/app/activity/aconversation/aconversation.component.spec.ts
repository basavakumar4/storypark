import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AconversationComponent } from './aconversation.component';

describe('AconversationComponent', () => {
  let component: AconversationComponent;
  let fixture: ComponentFixture<AconversationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AconversationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AconversationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
