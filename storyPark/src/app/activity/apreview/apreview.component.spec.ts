import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApreviewComponent } from './apreview.component';

describe('ApreviewComponent', () => {
  let component: ApreviewComponent;
  let fixture: ComponentFixture<ApreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
